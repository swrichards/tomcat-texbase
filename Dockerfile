FROM tomcat:6
MAINTAINER Sidney Richards <s.w.richards@gmail.com>

RUN apt-get update -y
RUN apt-get install -y pandoc texlive-full curl maven openjdk-7-jdk
